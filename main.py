# from fastapi import FastAPI, Request
# from database import get_all_cars
# from database import get_car_info
#
#
# app = FastAPI()
#
# @app.get("/")
# def read_root():
#     return "Мой первый проект"
#
# @app.get("/cars")
# def read_cars():
#     cars = get_all_cars()
#     cars_name = []
#     for car in cars:
#         cars_name.append(car[1])
#
#
#     return cars_name
#
#
# @app.get("/car_info")
# def read_car_info(car_model: str):
#     car_info = get_car_info(car_model)
#     if car_info:
#         return car_info
#     else:
#         return {"error": "Car not found"}
#

from fastapi import FastAPI
from database import get_all_cars, get_car_info

app = FastAPI()

@app.get("/")
def read_root():
    return "Мой первый проект"

@app.get("/cars")
def read_cars():
    cars = get_all_cars()
    cars_name = [car[1] for car in cars]
    return cars_name

@app.get("/car_info")
def read_car_info(car_model: str):
    car_info = get_car_info(car_model)
    if car_info:
        return car_info
    else:
        return {"error": "Car not found"}

